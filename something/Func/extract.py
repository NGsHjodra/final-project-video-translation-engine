import moviepy.editor as mp
import os

def extract(filepath :str):
    #filename = filename.encode('unicode_escape').decode()
    
    my_clip = mp.VideoFileClip(f"uploads/{filepath}")
    filepath = filepath.split('.')[0]
    my_clip.audio.write_audiofile(f"In_progress/{filepath}.wav")