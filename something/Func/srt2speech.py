import os

import pysrt
from pydub import AudioSegment
from google.cloud import texttospeech

#copy from chinese dude work (pydub thing) maybe in another file also
#keep var yuyin and some comment as it be
#chineses 1 word a day     yuyin means speech

def srtTOspeech():

    wavpath = "wav/"

    check_mkdir(wavpath)

    mysrt = pysrt.open("In_progress/thaisubtitle.srt")

    outwav = AudioSegment.empty()
    outwav += AudioSegment.silent(duration=mysrt[0].start.ordinal)

    i = 1
    for part in mysrt:
        file_name = str(i) + "-" + part.text + ".wav"
        tospeech(part.text, file_name)
        yuyin = AudioSegment.from_file("wav/" + file_name)

        #set time sync
        try:
            yuyinduration = yuyin.duration_seconds*1000
            originduration = mysrt[i].end.ordinal-mysrt[i].start.ordinal
            if yuyinduration < originduration :
                outwav += AudioSegment.silent(duration=originduration-yuyinduration)
            else:
                speed = yuyinduration/originduration
                yuyin = speed_change(yuyin, speed)
        except:
            continue

        outwav += yuyin
        try:
            silent_time = mysrt[i].start.ordinal-int(outwav.duration_seconds*1000)
            outwav += AudioSegment.silent(duration=silent_time)
        except:
            continue
        i += 1

    outwav.export("In_progress/out.mp3", format="mp3")

def speed_change(sound, speed=1.0):
    rate = sound._spawn(sound.raw_data, overrides={
        "frame_rate": int(sound.frame_rate * speed)
      })
    return rate.set_frame_rate(sound.frame_rate)


def tospeech(text, name="name"):
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.join(os.path.realpath(os.path.dirname(__file__)),'astrodev-googleapi-key.json')

    client = texttospeech.TextToSpeechClient()

    synthesis_input = texttospeech.SynthesisInput(text=text)

    voice = texttospeech.VoiceSelectionParams(
        language_code="th-TH", ssml_gender=texttospeech.SsmlVoiceGender.NEUTRAL
    )

    audio_config = texttospeech.AudioConfig(
        #pitch=-20, 
        audio_encoding=texttospeech.AudioEncoding.MP3
    )

    response = client.synthesize_speech(
        input=synthesis_input, voice=voice, audio_config=audio_config
    )

    with open(f"wav/{name}", "wb") as out:
        out.write(response.audio_content)    

def check_mkdir(path):
    """ 检查一个目录是否存在，不存在则新建 """
    if not os.path.exists(path):
        os.makedirs(path)
