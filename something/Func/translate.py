import pysrt
from googletrans import Translator

def translate_srt():
    translator = Translator()
    subs = pysrt.open("In_progress/subtitle.srt")

    for i,sub in enumerate(subs):
        subs[i].text = translator.translate(sub.text, src = 'en', dest = 'th').text

    subs.save('In_progress/thaisubtitle.srt', encoding='utf-8')