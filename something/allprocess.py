from Func import concat, create_srt, extract, translate, srt2speech
import os
import logging

logging.basicConfig(filename="log.txt", level=logging.DEBUG,
                    format="%(asctime)s %(message)s", filemode="w")
logging.debug("Logging test...")
logging.info("The program is working as expected")
logging.warning("The program may not function properly")
logging.error("The program encountered an error")
logging.critical("The program crashed")

def runall(filename : str):
    filename = os.path.basename(filename)

    extract.extract(filename)
    print("extracted file")
    create_srt.create_srt(filename)
    print('created srt')
    translate.translate_srt()
    print('translated srt')
    srt2speech.srtTOspeech()
    print('making voice')
    concat.concat(filename)