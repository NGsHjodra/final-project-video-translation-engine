import os
from flask import Flask, render_template, request, redirect, url_for, send_file
from flask_dropzone import Dropzone
import time
from allprocess import runall

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config.update(
    UPLOADED_PATH= os.path.join(basedir,'uploads'),
    DROPZONE_MAX_FILE_SIZE = 1024,
    DROPZONE_TIMEOUT = 5*60*1000)

dropzone = Dropzone(app)
@app.route('/')
def index():
    directorys = ['uploads','wav','In_progress']
    for directory in directorys:
        for root, dirs, files in os.walk(directory):
            for filename in files:
                os.remove(os.path.join(root, filename))
    return render_template('index.html')

@app.route("/get_files/<path:filename>")
def get_files(filename):
    """Download a file."""
    print(filename)
    return send_file(filename, as_attachment=True)

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        for f in request.files.getlist('file'):
            f.save(os.path.join(app.config['UPLOADED_PATH'], f.filename))
    return '1'

@app.route('/test')
def test():
    return render_template('main.html')

@app.route('/working')
def working():
    nameoffile = ''
    directory = 'uploads'
    for root, dirs, files in os.walk(directory):
        for filename in files:
            nameoffile = (os.path.join(directory, filename))
    print(f"working one {nameoffile}")
    runall(nameoffile)
    return '1'

@app.route('/done')
def done():
    return render_template('done.html',filename = 'final.mp4')

if __name__ == "__main__":
    app.run(debug=True, port=8000)